﻿using UnityEngine;
using System.Collections;

public class TraversablePlatform : MonoBehaviour {
    public GameObject platformCollider;
    private bool playerPressingDown;
	// Use this for initialization
	void Start () {
	
	}
    void OnTriggerStay2D(Collider2D playerCollider)
    {
        if (playerPressingDown)
        {
            Physics2D.IgnoreCollision(platformCollider.transform.GetComponent<Collider2D>(), playerCollider);
        }
        
    }
    void OnTriggerExit2D(Collider2D playerCollider)
    {
        Physics2D.IgnoreCollision(platformCollider.transform.GetComponent<Collider2D>(), playerCollider,false);
    }
	// Update is called once per frame
    void Update()
    {
        // Si l'utilisateur pèse vers le bas
        if (Input.GetAxis("Vertical") < 0)
        {
            playerPressingDown = true;
        }
        else
        {
            playerPressingDown = false;
        }
    }
}

﻿//using UnityEngine;
//using System.Collections;

//public class GroundSpawn : MonoBehaviour {


//    public Section section1;
//    public Section section2;
//    public Section section3;
//    //public List<Section> usableSections = new List<Section>();
//    private Vector3 nextElemPosition;
//    private GameObject[] tabSections;
//    private int tabSectionIndex;
//    private int randomSectionIndex;
//    private float finSection = 18;
//    // Use this for initialization
//    void Start()
//    {
//        section1 = new Section();
//        section2 = new Section();
//        section3 = new Section();

//        tabSectionIndex = 0;
//        tabSections = new GameObject[7];

//        section1.section = Resources.Load("Prefab/Section_Flat") as GameObject;
//        section1.length = 18;
//       // usableSections.Add(section1);

//        //section2.section = Resources.Load("Prefab/Section2") as GameObject;
//        //section2.length = 18;
//        //usableSections.Add(section2); 

//        //section3.section = Resources.Load("Prefab/Section3") as GameObject;
//        // section3.length = 18;
//        // usableSections.Add(section3); 

//        // Création de la première plate-forme du jeu.
//        tabSections[tabSectionIndex] = Instantiate(section1.section, new Vector3(nextElemPosition.x, 0), Quaternion.identity) as GameObject;
//        // tabSections[tabSectionIndex+1] = Instantiate(section1.section, new Vector3(nextElemPosition.x+18, 0), Quaternion.identity) as GameObject;
//        //tabSections[tabSectionIndex+2] = Instantiate(section1.section, new Vector3(nextElemPosition.x+36, 0), Quaternion.identity) as GameObject;
//        tabSectionIndex += 3;
//    }

//    void LateUpdate()
//    {
//        // Vitesse de défilement x et y de la caméra
//        float x = 0.05F;
//        float y = 0;
//        transform.position = new Vector3(transform.position.x + x, transform.position.y + y, transform.position.z);
//        // Arrondis la caméra à l'entier le plus bas
//        float cameraPositionX = Mathf.Floor(transform.position.x);

//        //Debug.Log(cameraPositionX + " " + finSection);
//        // Génère la prochaine section dès qu'on arrive à 20 espaces de la fin de la section en cours
//        if (cameraPositionX > finSection - 60)
//        {

//            finSection += addSection();
//        }
//    }

//    /*****
//     *Crée la prochaine section. affiché à l'écran
//     * */
//    public float addSection()
//    {


//        // Choisi aléatoirement la section à générer
//        randomSectionIndex = Random.Range(0, 1);

//        // détermine la position de la prochaine section selon la taille de la section qui va être mise
//        nextElemPosition.x += usableSections[randomSectionIndex].length;   //On prend maintenant pour acquis que toutes les sections sont de la même taille.


//        // Détruis les sections plus vielles que 7 sections
//        if (tabSections[tabSectionIndex] != null)
//        {
//            Destroy(tabSections[tabSectionIndex]);
//        }
//        // Création de la section
//        //Vector3 p = camera.ViewportToWorldPoint(new Vector3(1, 1, camera.nearClipPlane));

//        tabSections[tabSectionIndex] = Instantiate(usableSections[randomSectionIndex].section, new Vector3(finSection, 0, 0), Quaternion.identity) as GameObject; //nextElemPosition.x, 0
//        tabSectionIndex++;
//        // Retour au début du tableau qui contient les sections les plus récentes lorsqu'on arrive à la fin
//        if (tabSectionIndex == tabSections.Length)
//        {
//            tabSectionIndex = 0;
//        }
//        return usableSections[randomSectionIndex].length;
//    }
//}

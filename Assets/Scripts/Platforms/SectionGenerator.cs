﻿using UnityEngine;
using System.Collections.Generic;

public class SectionGenerator : MonoBehaviour
{
    public float ScrollingSpeed = 2.5f;
    public GameObject[] tabSections;
    public int[] sectionsLength;

    private float _previousSectionLength;
    private float _elapsedTime = 0f;

    
	
    void Start ()
    {
        
        _previousSectionLength = sectionsLength[0];

        int section2Index = Random.Range(0, tabSections.Length);
        int section3Index = Random.Range(0, tabSections.Length);
        // Initialize the starting sections, you need 3 sections manually initialised before the generator can create the rest.
        Instantiate(tabSections[0], new Vector3(0, 0), Quaternion.identity);
        Instantiate(tabSections[section2Index], new Vector3( _previousSectionLength, 0), Quaternion.identity);
        Instantiate(tabSections[section3Index], new Vector3(2 * _previousSectionLength, 0), Quaternion.identity);
	}

    void Update()
    {
        if (GameManager.Instance.MatchHasStarted)
        {
            _elapsedTime += Time.deltaTime;

            if (_elapsedTime * ScrollingSpeed >= _previousSectionLength)
            {
              float offset = _elapsedTime * ScrollingSpeed - _previousSectionLength;
             int randomIndex = Random.Range(0, tabSections.Length);
             _previousSectionLength = sectionsLength[randomIndex];

             Instantiate(tabSections[randomIndex], new Vector3(gameObject.transform.position.x - offset, 0, 30), Quaternion.identity);

             _elapsedTime = 0f;
           }
        }
    }
}


﻿using UnityEngine;
using System.Collections;

public class TraversablePlatformBottom : MonoBehaviour {
    public GameObject platformCollider;
	// Use this for initialization
	void Start () {
	
	}
    void OnTriggerEnter2D(Collider2D playerCollider)
    {
         Physics2D.IgnoreCollision(platformCollider.transform.GetComponent<Collider2D>(), playerCollider);
    }
    void OnTriggerExit2D(Collider2D playerCollider)
    {
        Physics2D.IgnoreCollision(platformCollider.transform.GetComponent<Collider2D>(), playerCollider, false);
    }
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;
// Efface tout ce qui entre en contact avec lui
public class GarbageCollector : MonoBehaviour {
    void OnTriggerEnter2D(Collider2D other)
    {
        // si l'objet a un parent
        if (other.gameObject.transform.parent)
        {
            Destroy(other.gameObject.transform.parent.gameObject);
        }
        else
        {
            Destroy(other.gameObject);
        }
    }
}

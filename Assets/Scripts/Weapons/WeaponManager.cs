﻿using UnityEngine;
using System.Collections;

public class WeaponManager : MonoBehaviour
{
    public enum WeaponTypes { None, Melee, Ranged }

    private string currentWeaponName;
    private GameObject currentWeapon;
    private Weapon currentWeaponScript;

    private WeaponTypes _currentWeaponType;

	void Start ()
    {
        currentWeaponName = "";
        currentWeapon = null;
        currentWeaponScript = null;
	}

    public WeaponTypes CurrentWeaponType
    {
        get { return _currentWeaponType; }
    }

    // Weapon weapon;
    public void fireWeapon()
    {
        // TODO: BAD, refactor this

        if (currentWeaponName.Equals(WeaponName.GREEN_BAZOOKA))
        {
            Green_Bazooka[] greenBazookas = GetComponentsInChildren<Green_Bazooka>(true);
            Green_Bazooka greenBazooka = greenBazookas[0];
            greenBazooka.fireWeapon();
        }
        else if (currentWeaponName == WeaponName.KNIFE)
        {
            Knife[] knives = GetComponentsInChildren<Knife>(true);
            Knife knife = knives[0];
            knife.fireWeapon();
        }

        else if (currentWeaponName == WeaponName.PAN)
        {
            Pan[] pans = GetComponentsInChildren<Pan>(true);
            Pan pan = pans[0];
            pan.fireWeapon();
        }
    }

    public void isWeaponActive(bool active)
    {
       // Debug.Log("je suis entré");
      //  Debug.Log("CurrentweaponScript " + currentWeaponScript.);
      //  if (currentWeaponScript != null)
       // {
            currentWeaponScript.setWeaponActive(active);
       // }
        
    }
    public void aquireWeapon(string weaponName)
    {
        currentWeaponName = weaponName;
        currentWeapon = null;
        currentWeaponScript = null;
        removeWeapons();

        // Pas très beau mais ça fait pour l'instant
         if (weaponName.Equals(WeaponName.KNIFE))
        {
            _currentWeaponType = WeaponTypes.Melee;
            Knife[] knives = GetComponentsInChildren<Knife>(true);
            Knife knife = knives[0];
            currentWeapon = knife.transform.parent.gameObject; // get the object and not just the collider which the script is attached to.
            currentWeaponScript = knife;//currentWeapon.GetComponent<Knife>();
        }
        else if (weaponName.Equals(WeaponName.GREEN_BAZOOKA))
         {
             _currentWeaponType = WeaponTypes.Ranged;
             Green_Bazooka[] greenBazookas = GetComponentsInChildren<Green_Bazooka>(true);
             Green_Bazooka greenBazooka = greenBazookas[0];
             currentWeapon = greenBazooka.transform.parent.gameObject; // get the object and not just the collider which the script is attached to.
             currentWeaponScript = greenBazooka;//currentWeapon.GetComponent<Green_Bazooka>();
         }

         else if (weaponName.Equals(WeaponName.PAN))
         {
             _currentWeaponType = WeaponTypes.Melee;
             Pan[] pans = GetComponentsInChildren<Pan>(true);
             Pan pan = pans[0];
             currentWeapon = pan.transform.parent.gameObject; // get the object and not just the collider which the script is attached to.
             currentWeaponScript = pan;//currentWeapon.GetComponent<Pan>();
         }

         currentWeapon.SetActive(true);

         if (currentWeapon == null)
         {
             Debug.Log("Erreur dans la classe WeaponManager, arme nulle");
         }
    }
    private void removeWeapons()
    {
        _currentWeaponType = WeaponTypes.None;

        GameObject playerHand = this.gameObject;
        for (int i = 0; i < playerHand.transform.childCount; i++)
        {
            GameObject child = playerHand.transform.GetChild(i).gameObject;
            if (child != null)
                child.SetActive(false);
        }
    }
}

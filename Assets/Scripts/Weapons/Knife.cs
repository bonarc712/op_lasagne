﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(KnifeSound))]
public class Knife : Weapon
{
    public Character _player;
    public float KnockbackValue = 50f;
    public float yDirection = 0.2f;
   // private bool isActive = false; ///////////// activate/desactivate the collider for the weapon. inactive when not attacking

    private KnifeSound _knifeSound;

    void Awake()
    {
        _knifeSound = GetComponent<KnifeSound>();
    }
    //public void isAttacking(bool isAttacking)
  //  {
    //    this.isActive = isAttacking;
    //}

    public void OnCollisionEnter2D(Collision2D coll)
    {
        // si activé
        if (coll.gameObject.tag == "Character")
        {
            Debug.Log("character hit");
        }
        Debug.Log("something else");
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("inactif");
        if (isActive)
        {
            Debug.Log("actif");
            //si arme -- désactive l'attaque
            if (other.gameObject.tag == "Character")
            {
                float directionModifier = _player.FacingRight ? 1f : -1f;
                Character otherPlayer = other.transform.gameObject.GetComponent<Character>();

                otherPlayer.receiveHit(new Vector2(directionModifier, yDirection) * KnockbackValue);
                _knifeSound.PlayImpact();
            }
        }
        
        
    }
    // Enlever si je réussis à faire marcher le OntriggerEnter
    public void fireWeapon()
    {
        _knifeSound.PlaySwing();

        Vector2 pos = _player.transform.position;
        pos.y += 1f;

        float directionModifier = _player.FacingRight ? 1f : -1f;

        RaycastHit2D[] targets = Physics2D.RaycastAll(pos, new Vector2(directionModifier, 0f), 2f);

        foreach (RaycastHit2D target in targets)
        {
            if (target.transform.gameObject.tag == "Character" && target.transform.gameObject != _player.gameObject)
            {
                Character otherPlayer = target.transform.gameObject.GetComponent<Character>();

                otherPlayer.receiveHit(new Vector2(directionModifier, yDirection) * KnockbackValue);
                _knifeSound.PlayImpact();
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System;

public class GreenBazookaBullet : MonoBehaviour
{
    public Action OnCharacterHit;

    public float Speed = 500f;

    private Character _owner;
    private float _knockbackValue;
    private float _knockbackYDirection;

	void Start ()
    {
        rigidbody2D.AddRelativeForce(new Vector2(-Speed, 0f));

        Invoke("KillAfterTime", 3f);
	}

    public void SetProperties(Character owner, float knockbackValue, float knockbackYDirection)
    {
        _owner = owner;
        _knockbackValue = knockbackValue;
        _knockbackYDirection = knockbackYDirection;
    }

    private void KillAfterTime()
    {
        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Character" && other.gameObject != _owner.gameObject)
        {
            Character otherCharacter = other.gameObject.GetComponent<Character>();

            Vector2 direction = -transform.right;

            direction.y = _knockbackYDirection;

            otherCharacter.ReceiveProjectileHit(direction * _knockbackValue);

            if (OnCharacterHit != null)
            {
                OnCharacterHit();
            }

            Destroy(gameObject);
        }
    }
}

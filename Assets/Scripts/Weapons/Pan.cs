﻿using UnityEngine;
using System.Collections;

public class Pan : Weapon {

    public Character _player;
    private float KnockbackValue = 200f;
    public float yDirection = 0.2f;

   // private KnifeSound _knifeSound; // TODO put a pan sound instead

    void Awake()
    {
       // _knifeSound = GetComponent<KnifeSound>();
    }

    public void fireWeapon()
    {
        
        //_knifeSound.PlaySwing();

        Vector2 pos = _player.transform.position;
        pos.y += 1f;

        float directionModifier = _player.FacingRight ? 1f : -1f;

        RaycastHit2D[] targets = Physics2D.RaycastAll(pos, new Vector2(directionModifier, 0f), 2f);

        foreach (RaycastHit2D target in targets)
        {
            if (target.transform.gameObject.tag == "Character" && target.transform.gameObject != _player.gameObject)
            {
                Character otherPlayer = target.transform.gameObject.GetComponent<Character>();

                otherPlayer.ReceiveCriticalHit(new Vector2(directionModifier, yDirection) * KnockbackValue, 10);
                
               // _knifeSound.PlayImpact();
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;


public static class WeaponName {

    public static readonly string KNIFE = "Knife";
    public static readonly string GREEN_BAZOOKA = "green_bazooka";
    public static readonly string PAN = "pan";
}

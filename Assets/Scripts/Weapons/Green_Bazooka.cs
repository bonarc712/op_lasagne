﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SnotGunSound))]
public class Green_Bazooka : Weapon
{
    public Character Player;
    public GreenBazookaBullet bullet;
    public float KnockbackValue = 50f;
    public float yDirection = 0.2f;

    public Vector2 BulletOffset;

    private SnotGunSound _snotGunSound;

    void Awake()
    {
        _snotGunSound = GetComponent<SnotGunSound>();
    }

    public void fireWeapon()
    {
        _snotGunSound.PlayShoot();

        Transform bazookaTransform = this.gameObject.transform;
        GreenBazookaBullet bulletClone = Instantiate(bullet, new Vector3(Player.transform.position.x + BulletOffset.x, Player.transform.position.y + BulletOffset.y, 0), Quaternion.identity) as GreenBazookaBullet;

        bulletClone.SetProperties(Player, KnockbackValue, yDirection);

        float zRotation = Player.FacingRight ? 180f : 0f;

        bulletClone.transform.localEulerAngles = new Vector3(bulletClone.transform.localEulerAngles.x, bulletClone.transform.localEulerAngles.y, zRotation);
        bulletClone.OnCharacterHit = _snotGunSound.PlayImpact;
    }
}

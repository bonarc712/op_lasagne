﻿using UnityEngine;
using System.Collections;

public class Levitate : MonoBehaviour
{
    public float MaximumOffset = 0.3f;
    public float Speed = 1f;

    private float _elapsedTime = 0f;
    private Transform _transform;

    void Awake()
    {
        _transform = GetComponent<Transform>();
    }
	
	void Update ()
    {
        _elapsedTime += Time.deltaTime;

        float newY = Mathf.Lerp(-MaximumOffset, MaximumOffset, (Mathf.Sin(_elapsedTime * Speed) + 1) / 2);

        // Even though the domain of a sinus function is R, we reset the time after the first phase so that we don't end up with absurdly high (and less precise) values when the game runs for too long
        if (_elapsedTime > Mathf.PI * 2)
        {
            _elapsedTime -= Mathf.PI * 2;
        }

        _transform.localPosition = new Vector3(_transform.localPosition.x, newY, _transform.localPosition.z);
	}
}

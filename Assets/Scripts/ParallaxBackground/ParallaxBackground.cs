﻿using UnityEngine;
using System.Collections;
using System;

public class ParallaxBackground : MonoBehaviour
{
    public enum Directions { Left, Right, Up, Down }

    [Serializable]
    public struct Background
    {
        public SpriteRenderer Sprite;
        public float Speed;
    }

    public Directions Direction;
    public Background[] Backgrounds;

    private float _cameraHeight;
    private float _cameraWidth;
    private Transform[] _backgroundTransforms;
    private float[] _backgroundWidths;
    private float[] _backgroundHeights;

    void Awake()
    {
        if (Backgrounds.Length == 0)
	    {
            Debug.LogWarning("You need to assign some backgrounds to the parallax.");
            return;
	    }

        _backgroundTransforms = new Transform[Backgrounds.Length];
        _backgroundWidths = new float[Backgrounds.Length];
        _backgroundHeights = new float[Backgrounds.Length];

        for (int i = 0; i < Backgrounds.Length; i++)
        {
            _backgroundWidths[i] = Backgrounds[i].Sprite.bounds.extents.x * 2;
            _backgroundHeights[i] = Backgrounds[i].Sprite.bounds.extents.y * 2;
            _backgroundTransforms[i] = Backgrounds[i].Sprite.GetComponent<Transform>();
        }

        _cameraHeight = 2f * Camera.main.orthographicSize;
        _cameraWidth = Camera.main.aspect * _cameraHeight;
    }

    void Update()
    {
        if (!GameManager.Instance.MatchHasStarted) return;

        for (int i = 0; i < Backgrounds.Length; i++)
        {
            _backgroundTransforms[i].localPosition = GetNewPosition(i);
        }
    }

    private Vector3 GetNewPosition(int index)
    {
        Vector3 newPos = _backgroundTransforms[index].localPosition;

        float border = 0f;

        switch (Direction)
        {
            case Directions.Left:
                border = (_cameraWidth + _backgroundWidths[index]) / 2;

                if (_backgroundTransforms[index].localPosition.x <= -border)
                {
                    newPos.x = border;
                }
                else
                {
                    newPos.x = _backgroundTransforms[index].localPosition.x - Time.deltaTime * Backgrounds[index].Speed;
                }

                break;
            case Directions.Right:
                border = (_cameraWidth + _backgroundWidths[index]) / 2;

                if (_backgroundTransforms[index].localPosition.x >= border)
                {
                    newPos.x = -border;
                }
                else
                {
                    newPos.x = _backgroundTransforms[index].localPosition.x + Time.deltaTime * Backgrounds[index].Speed;
                }

                break;
            case Directions.Up:
                border = (_cameraHeight + _backgroundHeights[index]) / 2;

                if (_backgroundTransforms[index].localPosition.y >= border)
                {
                    newPos.y = -border;
                }
                else
                {
                    newPos.y = _backgroundTransforms[index].localPosition.y + Time.deltaTime * Backgrounds[index].Speed;
                }

                break;
            case Directions.Down:
                border = (_cameraHeight + _backgroundHeights[index]) / 2;

                if (_backgroundTransforms[index].localPosition.y <= -border)
                {
                    newPos.y = border;
                }
                else
                {
                    newPos.y = _backgroundTransforms[index].localPosition.y - Time.deltaTime * Backgrounds[index].Speed;
                }

                break;
        }

        return newPos;
    }
}

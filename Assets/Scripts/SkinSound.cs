﻿using UnityEngine;
using System.Collections;

public class SkinSound : MonoBehaviour
{
    public string CharacterSelect;
    public string SkinSelectPrefix;

    public void PlaySelectSkin(int skinIndex)
    {
        string skin = SkinSelectPrefix + (char)('a' + skinIndex);

        Fabric.EventManager.Instance.PostEvent(CharacterSelect, Fabric.EventAction.SetSwitch, skin);
        Fabric.EventManager.Instance.PostEvent(CharacterSelect);
    }
}

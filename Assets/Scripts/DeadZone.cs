﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SawSound))]
public class DeadZone : MonoBehaviour
{
    private const float DEAD_TIME = 2f;

    private SawSound _sawSound;

    void Awake()
    {
        _sawSound = GetComponent<SawSound>();
    }

    void OnTriggerEnter2D(Collider2D triggeredCollider)
    {
        if (triggeredCollider.tag == "Character")
        {
            Character character = triggeredCollider.gameObject.GetComponent<Character>();

            //TODO: Move all of this directly into the character. This is not the business of the deadzone or the wall to manage the deaths

            character.Die();

            if (_sawSound != null)
            {
                _sawSound.PlayKill();
            }
            
            character.gameObject.SetActive(false); // Temporary, until ragdoll

            Player player = character.Player;

            ScoreManager.Instance.updatePlayerScore(player, -1);
            

            // Si le joueur a encore des vies, il respawn.
            if (player.LifeCount > 0)
            {
                StartCoroutine(RevivePlayer(character));
                
            }
            else
            {
                ScoreManager.Instance.terminatePlayer(player);
                Destroy(character.gameObject);
            }
        }
    }

    private IEnumerator RevivePlayer(Character character)
    {
        yield return new WaitForSeconds(DEAD_TIME);

        character.gameObject.SetActive(true); // Temporary, until ragdoll

        character.Reset();
        GameManager.SetCharacterPosition(character);
        BouncyWall.Instance.resetWallKnockback(character.gameObject);
    }
}

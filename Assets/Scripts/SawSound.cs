﻿using UnityEngine;
using System.Collections;

public class SawSound : MonoBehaviour
{
    public string Kill;

    public void PlayKill()
    {
        Fabric.EventManager.Instance.PostEvent(Kill);
    }
}

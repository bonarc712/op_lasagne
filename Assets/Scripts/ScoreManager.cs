﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {
    public Text[] LifeAmounts;
    public Image[] LifeIcons;

    private static ScoreManager _instance;

    public static ScoreManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private Vector3[] _playerLifePositions;
    private Vector3[] _lifeIconsInitialScales;
    private Vector3[] _playerLifeInitialScales;

    public Vector3[] PlayerLifePositions
    {
        get { return _playerLifePositions; }
    }

    private void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        _playerLifePositions = new Vector3[LifeIcons.Length];
        _lifeIconsInitialScales = new Vector3[LifeIcons.Length];
        _playerLifeInitialScales = new Vector3[LifeIcons.Length];

        for (int i = 0; i < LifeIcons.Length; i++)
        {
            _playerLifePositions[i] = LifeIcons[i].transform.position;
            _lifeIconsInitialScales[i] = LifeIcons[i].transform.localScale;
            _playerLifeInitialScales[i] = LifeAmounts[i].transform.localScale;
        }
    }

    public void updatePlayerScore(Player player, int amount)
    {
        player.LifeCount += amount;

        if (amount < 0)
        {
            RefreshVisualPlayerScore(player);
        }
    }

    public void terminatePlayer(Player player)
    {
        LifeAmounts[player.Number - 1].text = "GG";
    }

    // Refreshes the visual display of the lives when the physical powerup connect the top HUD
    public void RefreshVisualPlayerScore(Player player)
    {
        LifeAmounts[player.Number - 1].text = player.LifeCount.ToString();
    }

    public void PulseLifeIcon(int playerIndex)
    {
        StartCoroutine(PulseLifeIconCoroutine(playerIndex));
    }

    private IEnumerator PulseLifeIconCoroutine(int playerIndex)
    {
        Vector3 normalScale = _lifeIconsInitialScales[playerIndex];
        Vector3 pulsedScale = normalScale + new Vector3(0.1f, 0.1f, 0.1f);

        Vector3 normalTextScale = _playerLifeInitialScales[playerIndex];
        Vector3 pulsedTextScale = normalTextScale + new Vector3(0.1f, 0.1f, 0.1f);

        float ratio = 0f;

        while (ratio < 1f)
        {
            ratio += Time.deltaTime / 0.05f;

            LifeIcons[playerIndex].transform.localScale = Vector3.Lerp(normalScale, pulsedScale, ratio);
            LifeAmounts[playerIndex].transform.localScale = Vector3.Lerp(normalTextScale, pulsedTextScale, ratio);

            yield return null;
        }

        ratio = 0f;

        while (ratio < 1f)
        {
            ratio += Time.deltaTime / 0.05f;

            LifeIcons[playerIndex].transform.localScale = Vector3.Lerp(pulsedScale, normalScale, ratio);
            LifeAmounts[playerIndex].transform.localScale = Vector3.Lerp(pulsedTextScale, normalTextScale, ratio);

            yield return null;
        }
    }
}

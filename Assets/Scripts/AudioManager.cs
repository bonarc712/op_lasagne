﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour
{
    private static AudioManager _instance;

    // This is needed because the Fabric's Audio prefab doesn't destroy on load, but we need to put it in every scene for testing purposes
    void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }
}

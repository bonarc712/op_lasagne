﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{

    public Character SelectedCharacter;

    private string playerName;

    public string PlayerName
    {
        get { return playerName; }
        set { playerName = value; }
    }

    public int Number
    {
        get;
        set;
    }

    private int _skin;

    public int Skin
    {
        get { return _skin; }
        set { _skin = value; }
    }


    public int LifeCount { get; set; }

    public void SpawnCharacter()
    {
        SelectedCharacter = Instantiate(SelectedCharacter, this.transform.position, Quaternion.identity) as Character;

        SelectedCharacter.Rig.SetSkin(_skin);
        SelectedCharacter.Player = this;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WallAnimation : MonoBehaviour
{
    public Transform[] Gears;
    public float RotationSpeed = 15;
    public Rigidbody2D EndJoint;

    private Vector3 _gearRotationAxis = new Vector3(0f, 0f, 1f);
    private float _armAnimationRatio = 0f;
    private bool _armIsExtending = false;
    private Vector2 _armInitialPosition;
    private Vector2 _armFinalPosition;

    private Dictionary<GameObject, EffectsManager.Effect> _woodSplinters;

    void Awake()
    {
        _armInitialPosition = EndJoint.position + new Vector2(0f, -0.2f);
        _armFinalPosition = _armInitialPosition + new Vector2(0f, 3.68f);

        _woodSplinters = new Dictionary<GameObject, EffectsManager.Effect>();
    }

    void Update()
    {
        if (!GameManager.Instance.MatchHasStarted) return;

        foreach (Transform gearTransform in Gears)
        {
            gearTransform.Rotate(_gearRotationAxis, -RotationSpeed * Time.deltaTime);
        }
    }

    void FixedUpdate()
    {
        if (!GameManager.Instance.MatchHasStarted) return;

        if (_armAnimationRatio < 1f)
        {
            _armAnimationRatio += Time.fixedDeltaTime / 0.25f;

            if (!_armIsExtending)
            {
                EndJoint.position = Vector2.Lerp(_armInitialPosition, _armFinalPosition, _armAnimationRatio);
            }
            else
            {
                EndJoint.position = (Vector2.Lerp(_armFinalPosition, _armInitialPosition, _armAnimationRatio));
            }
        }

        if (_armAnimationRatio >= 1f)
        {
            _armAnimationRatio = 0f;
            _armIsExtending = !_armIsExtending;
        }
    }

    void OnTriggerEnter2D(Collider2D triggeredCollider)
    {
        if (triggeredCollider.tag == "WoodPlatform")
        {
            Vector3 splinterPosition = new Vector3(gameObject.transform.position.x + 4, triggeredCollider.transform.position.y, triggeredCollider.transform.position.z - 10);

            EffectsManager.Effect effect = EffectsManager.Instance.GetOrCreateEffect("WoodSplinter", splinterPosition);

            effect.SetParentTransform(this.transform);

            EffectsManager.Instance.PlayEffect(effect);

            _woodSplinters.Add(triggeredCollider.gameObject, effect);
        }
    }

    void OnTriggerExit2D(Collider2D triggeredCollider)
    {
        if (triggeredCollider.tag == "WoodPlatform")
        {
            if (_woodSplinters.ContainsKey(triggeredCollider.gameObject))
            {
                EffectsManager.Instance.StopEffect(_woodSplinters[triggeredCollider.gameObject]);

                _woodSplinters.Remove(triggeredCollider.gameObject);
            }
        }
    }
}

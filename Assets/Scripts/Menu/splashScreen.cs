﻿using UnityEngine;
using System.Collections;

/*
 * programmer : Maxim Parisee
 * email : max.parisee@gmail.com
 * purpose : Script that will create the fading splash arts and the main menu.
 */

public class splashScreen : MonoBehaviour 
{
	//Public member variables
	//Fader related and background related
	public GameObject backgroundPlane;
	public Camera cam;
	public Texture[] splashImages;
	public Texture mainMenuImage;

	//Private member variables
	//Fader related variables
	private GameObject fader;
	private int currentImage = 0;
	private bool doneFading = false;

	//Menu related variables
	private float menuButtonHeight = 100;
	private float menuButtonWidth = 50;
	private int menuIndex = 0;

	// Use this for initialization
	void Start () 
	{
		backgroundPlane.transform.localScale = new Vector3 (cam.orthographicSize / 2 * (Screen.width / Screen.height), cam.orthographicSize / 2.0f, 1);
		initFader();
		StartCoroutine("splashControl");
	}
	//GUI function for the main menu
	void OnGUI()
	{
		if(doneFading && menuIndex == 0)
		{

			if(GUI.Button(new Rect(Screen.width/2 - menuButtonWidth, Screen.height /2 - 100, menuButtonHeight, menuButtonWidth), "Local play"))
			{
				//Start game
			}
			if(GUI.Button(new Rect(Screen.width/2 - menuButtonWidth, Screen.height /2 - 50, menuButtonHeight, menuButtonWidth), "Network play"))
			{
				//start network play
			}
			if(GUI.Button(new Rect(Screen.width/2 - menuButtonWidth, Screen.height /2, menuButtonHeight, menuButtonWidth), "Options"))
			{
				//Open the options and use menuIndex to determine the menu.
			}
			if(GUI.Button(new Rect(Screen.width/2 - menuButtonWidth, Screen.height /2 + 50, menuButtonHeight, menuButtonWidth), "Credits"))
			{
				//Roll credits!
			}
			if(GUI.Button(new Rect(Screen.width/2 - menuButtonWidth, Screen.height /2 + 100, menuButtonHeight, menuButtonWidth), "Exit game"))
			{
				//Leaving this place.
				Application.Quit();
			}
		}
	}
	//Function that will init the "fader" which is a plane that is in front of the camera to hide and show the view.
	void initFader()
	{
		//Creating the type of gameobject
		fader = GameObject.CreatePrimitive (PrimitiveType.Plane);

		//Position the plane in front of the background, and make it face the camera. Size is the viewing screen
		fader.transform.position = new Vector3 (-0.06f, 0, -0.13f);
		fader.transform.rotation = backgroundPlane.transform.rotation;
		fader.transform.localScale = new Vector3 (cam.orthographicSize / 2 * (Screen.width / Screen.height), cam.orthographicSize / 2.0f, 1);

		//Creating a transparent black shader with transparency to play with the alpha later on.
		fader.renderer.material.shader = Shader.Find("Transparent/Diffuse");
		Color temp = fader.renderer.material.color;
		temp.a = 1.0f;
		fader.renderer.material.color = temp;
	}

	//Function that takes a Texture and change the main_texture of the background plane.
	void backgroundImageChange(Texture in_texture)
	{
		Texture temp = backgroundPlane.renderer.material.mainTexture;
		temp = in_texture;
		backgroundPlane.renderer.material.mainTexture = temp;
	}

	//Coroutine that will slowly make the "fader" less transparent until clear.
	IEnumerator waitAndFadeOut()
	{
		for (int i = 0; i < 10; i++) 
		{
			if (fader.renderer.material.color.a > 0.0f) 
			{
				Color temp = fader.renderer.material.color;
				temp.a -= 0.1f;
				fader.renderer.material.color = temp;
			}
			yield return new WaitForSeconds(0.1f);
		}
	}

	//Coroutine that will slowly make the "fader" more transparent until solid.
	IEnumerator waitAndFadeIn()
	{
		for (int i = 0; i < 10; i++) 
		{
			if (fader.renderer.material.color.a < 1.0f) 
			{
				Color temp = fader.renderer.material.color;
				temp.a += 0.1f;
				fader.renderer.material.color = temp;
			}
			
			yield return new WaitForSeconds(0.1f);
		}
	}

	//Control coroutine for the splashScreen, it will take a total of 7 seconds per full transition.
	IEnumerator splashControl()
	{
		if (splashImages.Length >= currentImage) {
			yield return new WaitForSeconds (2.0f);
			//Time to hide our image
			StartCoroutine ("waitAndFadeOut");

			//Time to change our image //
			backgroundImageChange(splashImages[currentImage]);

			//Getting our plane transparent
			yield return new WaitForSeconds (4.0f);
			StartCoroutine ("waitAndFadeIn");

			currentImage++;

			if(splashImages.Length != currentImage)
			{
				StartCoroutine ("splashControl");
			}
			else
			{
				yield return new WaitForSeconds (1.0f);
				Debug.Log("ready to show the UI");
				backgroundImageChange(mainMenuImage);
				yield return new WaitForSeconds (2.0f);
				doneFading = true;
				DestroyObject(fader);
			}
		} 

	}
}

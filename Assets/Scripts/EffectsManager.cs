﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class EffectsManager : MonoBehaviour
{
    [Serializable]
    public class Effect
    {
        public string name;
        public ParticleSystem particleSystem;

        public void SetParentTransform(Transform parentTransform)
        {
            particleSystem.transform.parent = parentTransform;
        }
    }

    public Effect[] Effects;

    private static EffectsManager _instance;

    private Dictionary<string, ParticleSystem> _effects;
    private List<Effect> _cachedEffects;

    public static EffectsManager Instance
    {
        get { return _instance; }
    }
    
	void Awake ()
    {
        _instance = this;

        _effects = new Dictionary<string, ParticleSystem>();
        _cachedEffects = new List<Effect>();

        foreach (Effect effect in Effects)
        {
            _effects.Add(effect.name, effect.particleSystem);
        }
	}

    public Effect GetOrCreateEffect(string effectName, Vector3 position)
    {
        foreach (Effect cachedEffect in _cachedEffects)
        {
            if (cachedEffect.name == effectName && cachedEffect.particleSystem.isStopped)
            {
                cachedEffect.particleSystem.transform.position = position;
                cachedEffect.particleSystem.gameObject.SetActive(true);

                return cachedEffect;
            }
        }

        ParticleSystem newParticleSystem = Instantiate(_effects[effectName], position, Quaternion.identity) as ParticleSystem;

        Effect newEffect = new Effect() { name = effectName, particleSystem = newParticleSystem };

        _cachedEffects.Add(newEffect);

        return newEffect;
    }

    /*
    public ParticleSystem PlayInstantEffect(ParticleSystem effect, Vector3 location)
    {
        ParticleSystem effect = Instantiate(_effects[effectName], location, Quaternion.identity) as ParticleSystem;
        return effect;
    }*/

    public void PlayEffect(Effect effect)
    {
        effect.particleSystem.gameObject.SetActive(true);
        effect.particleSystem.Play();
    }

    public void StopEffect(Effect effect)
    {
        effect.particleSystem.Stop();
        //effect.particleSystem.gameObject.SetActive(false);
    }

    private void Destroy()
    {
        foreach (Effect effect in _cachedEffects)
        {
            Destroy(effect.particleSystem);
        }
    }
}

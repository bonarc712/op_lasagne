﻿using UnityEngine;
using System.Collections;

public class WeaponPowerupSound : MonoBehaviour
{
    public string Pickup;

    public void PlayPickup()
    {
        Fabric.EventManager.Instance.PostEvent(Pickup);
    }
}

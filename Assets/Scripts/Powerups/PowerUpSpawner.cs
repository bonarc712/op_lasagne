﻿using UnityEngine;
using System.Collections;

public class PowerUpSpawner : MonoBehaviour {

    public GameObject[] powerUp;
    public float spawnMin = 1.5f;
    public float spawnMax = 3f;
    // used to find out if there is space to spawn an actual powerup
	// Use this for initialization
	void Start () {
        Spawn();
	}
	

 void Spawn()
    {
        if (GameManager.Instance.MatchHasStarted)
        {
            int powerUpToSpawn = Random.Range(0, powerUp.Length);
            int spawningPositionY = Random.Range(2, 10);
            // the "start" position represent the bottom left of the powerup and the "end" the top right.
            float startXWeaponCollider = transform.position.x - powerUp[powerUpToSpawn].transform.localScale.x / 2;
            float endXWeaponCollider = transform.position.x + powerUp[powerUpToSpawn].transform.localScale.x / 2;
            float startYWeaponCollider = spawningPositionY - powerUp[powerUpToSpawn].transform.localScale.y / 2;
            float endYWeaponCollider = spawningPositionY + powerUp[powerUpToSpawn].transform.localScale.y / 2;

            // If you want to upgrade the collision detection, this line can be useful
            //Debug.DrawLine(new Vector2(startXWeaponCollider, startYWeaponCollider), new Vector2(endXWeaponCollider, endYWeaponCollider),Color.red,1);

            if (Physics2D.OverlapArea(new Vector2(startXWeaponCollider, startYWeaponCollider), new Vector2(endXWeaponCollider, endYWeaponCollider)) == null)
            {
                Instantiate(powerUp[powerUpToSpawn], new Vector2(transform.position.x, spawningPositionY), Quaternion.identity);
            }
        
        }

        Invoke("Spawn", Random.Range(spawnMin, spawnMax)); // spawnMin, spawnMax
    }
        
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(WeaponPowerupSound))]
public class PowerupCouteau : MonoBehaviour
{
    private WeaponPowerupSound _wpPowerupSound;

    void Awake()
    {
        _wpPowerupSound = GetComponent<WeaponPowerupSound>();
    }

    void OnTriggerEnter2D(Collider2D triggeredCollider)
    {
        if (triggeredCollider.tag == "Character")
        {
            Character character = triggeredCollider.gameObject.GetComponent<Character>();
            WeaponManager weaponManager = character.GetComponentInChildren<WeaponManager>();
           
            weaponManager.aquireWeapon(WeaponName.KNIFE);

            _wpPowerupSound.PlayPickup();

            Destroy(this.gameObject);
        }
    }
}

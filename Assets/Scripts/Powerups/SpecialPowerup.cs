﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Special Powerup (they do nothing yet)
/// </summary>
public class SpecialPowerup : MonoBehaviour
{
    public Transform Background;
    public float RotationSpeed = 500f;

    void Update()
    {
        Background.Rotate(0f, 0f, -RotationSpeed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D triggeredCollider)
    {
        // Si le powerup entre en collision avec le joueur, on augmente sa vie de 1 et on détruit le powerup
        if (triggeredCollider.tag == "Character")
        {
            // TODO: Increment the powerup count? Or immediatly use the powerup?

            /*
            Character character = triggeredCollider.gameObject.GetComponent<Character>();

            Player player = character.Player;

            ScoreManager.Instance.updatePlayerScore(player, 1);*/
        }
    }
}

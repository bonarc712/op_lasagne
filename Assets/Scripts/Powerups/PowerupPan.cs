﻿using UnityEngine;
using System.Collections;

public class PowerupPan : MonoBehaviour {

    private WeaponPowerupSound _wpPowerupSound;

    void Awake()
    {
        _wpPowerupSound = GetComponent<WeaponPowerupSound>();
    }

    void OnTriggerEnter2D(Collider2D triggeredCollider)
    {
        if (triggeredCollider.tag == "Character")
        {
            Character character = triggeredCollider.gameObject.GetComponent<Character>();
            WeaponManager weaponManager = character.GetComponentInChildren<WeaponManager>();

            weaponManager.aquireWeapon(WeaponName.PAN);

            _wpPowerupSound.PlayPickup();

            Destroy(this.gameObject);
        }
    }
}

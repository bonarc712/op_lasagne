﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RatioProgression : MonoBehaviour
{
    private Material _material;

    // For now, we can't put more than 2 choices in the shader somehow
    public enum Directions { Rightward, Upward }

    void Awake()
    {
        if (GetComponent<Renderer>() != null)
        {
            // GetComponent<Renderer>().material creates its own instance of the material, so it's not shared
            _material = GetComponent<Renderer>().material;
        }
        else if (GetComponent<Image>() != null)
        {
            // For the UI images, GetComponent<Image>().material is a shared material, so we have to clone it
            _material = Instantiate(GetComponent<Image>().material) as Material;
            GetComponent<Image>().material = _material;
        }
    }

    public void SetDirection(Directions direction)
    {
        _material.SetInt("_Direction", (int)direction);
    }

    public void SetCompletedRatio(float ratio)
    {
        _material.SetFloat("_Ratio", ratio);
    }

    public void SetRemainingTint(Color color)
    {
        _material.SetColor("_RemainingColor", color);
    }

    public void SetCompletedTint(Color color)
    {
        _material.SetColor("_CompletedColor", color);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/**
 * To prevent wall hugging, we add a bounce back everytime the player hit the right invisible wall. Everytime the player hit it, the knockback get stronger
 * */
public class BouncyWall : MonoBehaviour {
    // Force that push the player away from the wall (different from the wall bouciness) The more players collide with the right wall, the stronger they are pushed away.
    private float initialPushForce = -50;
    private Dictionary<GameObject, float> playersWallForce = new Dictionary<GameObject,float>();
	// Use this for initialization
	void Start () {
        _instance = this;
	}

    private static BouncyWall _instance;

    public static BouncyWall Instance
    {
        get
        {
            return _instance;
        }
    }

    public void resetWallKnockback(GameObject player)
    {
        Debug.Log("Player reset");
            playersWallForce[player] = initialPushForce;
        
    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        Debug.Log("entré confirmée");
        if (coll.gameObject.tag == "Character")
        {
            if (playersWallForce.ContainsKey(coll.gameObject))
            {
                playersWallForce[coll.gameObject] -= 25;
            }
            else
            {
                
                playersWallForce.Add(coll.gameObject, initialPushForce);
            }
            Character player = coll.transform.gameObject.GetComponent<Character>();
            player.colliderWithRightInvisibleWall(new Vector2(playersWallForce[coll.gameObject],5));
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}

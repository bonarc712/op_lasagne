﻿using UnityEngine;
using System.Collections;

public class KnifeSound : MonoBehaviour
{
    public string Swing;
    public string Impact;

    public void PlaySwing()
    {
        Fabric.EventManager.Instance.PostEvent(Swing);
    }

    public void PlayImpact()
    {
        Fabric.EventManager.Instance.PostEvent(Impact);
    }
}

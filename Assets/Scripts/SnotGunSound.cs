﻿using UnityEngine;
using System.Collections;

public class SnotGunSound : MonoBehaviour
{
    public string Shoot;
    public string Impact;

    public void PlayShoot()
    {
        Fabric.EventManager.Instance.PostEvent(Shoot);
    }

    public void PlayImpact()
    {
        Fabric.EventManager.Instance.PostEvent(Impact);
    }
}

﻿using UnityEngine;
using System.Collections;

public class LevelSound : MonoBehaviour
{
    public string ForestLevelMusic;

    public void PlayForestLevelMusic()
    {
        Fabric.EventManager.Instance.SetParameter("Music_Intensity", "Intensity", 0f, null);
        Fabric.EventManager.Instance.PostEvent(ForestLevelMusic);
    }
}

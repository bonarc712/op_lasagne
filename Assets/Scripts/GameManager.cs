﻿using UnityEngine;
using System.Collections;
using InputHandler;
using System;

[RequireComponent(typeof(LevelSound))]
public class GameManager : MonoBehaviour
{
    public Action OnMatchStarted;

    // TODO: Probably temporary, we shouldn't need a "Player" playerPrefab (at least used like that)
    public Player PlayerPrefab;

    public Player[] players;

    public int InitialLifeCount = 5;

    public float CountdownTime = 4.29f;
    public int FirstLevelIndex = 1;

    private static GameObject cam;
    private Character[] _characters;
    private int[] _skins;
    private LevelSound _levelSound;

    private float _remainingCountdown;

    private static GameManager _instance;

    public static GameManager Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            _instance = this;
            _levelSound = GetComponent<LevelSound>();

            cam = Camera.main.gameObject;

            if (Application.loadedLevel == FirstLevelIndex)
            {
                InitializePlayers();
                _levelSound.PlayForestLevelMusic();
                _remainingCountdown = CountdownTime;
            }
        }
    }

    void Update()
    {
        if (Application.loadedLevel == FirstLevelIndex && _remainingCountdown > 0f)
        {
            _remainingCountdown -= Time.deltaTime;

            if (_remainingCountdown <= 0f)
            {
                _remainingCountdown = 0f;
                _matchHasStarted = true;

                if (OnMatchStarted != null)
                {
                    OnMatchStarted();
                    OnMatchStarted = null;
                }
            }
        }
    }

    private bool _matchHasStarted;

    public bool MatchHasStarted
    {
        get { return _matchHasStarted; }
    }

	public static void SetCharacterPosition(Character character)
    {
		character.transform.position = new Vector3 (cam.transform.position.x, 15, 0);
	}

    private void InitializePlayers()
    {
        // TODO: Just a prototype made to not break anything, but eventually we shouldn't need to remake the Players array

        // When starting from the start
        if (_characters != null)
        {
            players = new Player[_characters.Length]; 

            for (int i = 0; i < _characters.Length; i++)
            {
                if (_characters[i] != null)
                {
                    players[i] = Instantiate(PlayerPrefab) as Player;
                    players[i].SelectedCharacter = _characters[i];
                    players[i].Skin = _skins[i];
                }
            }
        }

        // If starting directly from Tableau, the players array will contain what's in the editor (for debugging purposes alone)
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i] == null) continue;

            players[i] = Instantiate(players[i], new Vector3(cam.transform.position.x, 15, 0), Quaternion.identity) as Player;
            players[i].PlayerName = string.Format("Joueur{0}", i + 1);
            players[i].Number = i + 1;

            players[i].name = players[i].PlayerName;
            players[i].LifeCount = InitialLifeCount;

            players[i].SpawnCharacter();
        }
    }

    public void StartLevel(Character[] characters, int[] skins)
    {
        _characters = characters;
        _skins = skins;

        Application.LoadLevel("Tableau");
    }

    void OnLevelWasLoaded()
    {
        // Temporary
        if (cam == null && Application.loadedLevel == FirstLevelIndex)
        {
            cam = Camera.main.gameObject;
            InitializePlayers();
            _levelSound.PlayForestLevelMusic();
            _remainingCountdown = CountdownTime;
        }
    }
}

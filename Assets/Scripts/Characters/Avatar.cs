﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Avatar : MonoBehaviour
{
    public Image Banner;
    public Character SelectedCharacter;
    public CharacterRig Model;
}

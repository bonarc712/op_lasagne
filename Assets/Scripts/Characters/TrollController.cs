﻿using UnityEngine;
using System.Collections;
using InputHandler;

[RequireComponent(typeof(Character))]
public class TrollController : MonoBehaviour
{
    private Character _character;

    void Awake()
    {
        _character = GetComponent<Character>();
    }

    void Start()
    {
        InputManager.Instance.PushActiveContext("Troll" + _character.Player.Number, _character.Player.Number - 1);

        InputManager.Instance.AddCallback(_character.Player.Number - 1, HandleInput);
    }

    private void HandleInput(MappedInput input)
    {
        // Temporary
        if (this == null) return;

        if (!GameManager.Instance.MatchHasStarted || !_character.gameObject.activeSelf) return;

        if (input.Actions.Contains("Jump"))
        {
            _character.Jump();
        }

        if (input.Actions.Contains("Attack"))
        {
            _character.Attack();
        }

        if (input.Actions.Contains("Mock"))
        {
            _character.Mock();
        }

        float xAxisValue = 0f;

        if (input.Ranges.ContainsKey("MoveLeft"))
        {
            xAxisValue = -input.Ranges["MoveLeft"];
        }
        else if (input.Ranges.ContainsKey("MoveRight"))
        {
            xAxisValue = input.Ranges["MoveRight"];
        }

        _character.Move(xAxisValue);
    }
}

﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(SkinSound))]
[RequireComponent(typeof(Animator))]
public class CharacterRig : MonoBehaviour
{
    [Serializable]
    public struct Skin
    {
        public Material SkinMaterial;
        public string SkinName;
    }

    public Skin[] Skins;

    private SkinnedMeshRenderer[] _renderers;
    private int _skinIndex;
    private Animator _characterAnimator;
    private SkinSound _skinSound;

    public string SkinName
    {
        get { return Skins[_skinIndex].SkinName; }
    }

    public Animator CharacterAnimator
    {
        get { return _characterAnimator; }
    }

    void Awake()
    {
        _skinSound = GetComponent<SkinSound>();

        _renderers = transform.GetComponentsInChildren<SkinnedMeshRenderer>();
        _characterAnimator = GetComponent<Animator>();

        SetSkin(_skinIndex);
    }

    public void SetSkin(int index)
    {
        foreach (SkinnedMeshRenderer renderer in _renderers)
        {
            renderer.material = Skins[index].SkinMaterial;
        }

        _skinIndex = index;
    }

    public void SelectSkin()
    {
        _skinSound.PlaySelectSkin(_skinIndex);
    }
}

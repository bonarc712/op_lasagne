﻿using UnityEngine;
using System.Collections;

public class CharacterSound : MonoBehaviour
{
    public string Jump1;
    public string Jump2;
    public string Hurt;
    public string Die;
    public string Attack;
    public string Mock;
    public string Revive;
    public string Laugh;
    public string Select;

    public void PlayFirstJump()
    {
        Fabric.EventManager.Instance.PostEvent(Jump1);
    }

    public void PlaySecondJump()
    {
        Fabric.EventManager.Instance.PostEvent(Jump2);
    }

    public void PlayHurt()
    {
        Fabric.EventManager.Instance.PostEvent(Hurt);
    }

    public void PlayDie()
    {
        Fabric.EventManager.Instance.PostEvent(Die);
    }

    public void PlayAttack()
    {
        Fabric.EventManager.Instance.PostEvent(Attack);
    }

    public void PlayMock()
    {
        Fabric.EventManager.Instance.PostEvent(Mock);
    }

    public void PlayRevive()
    {
        Fabric.EventManager.Instance.PostEvent(Revive);
    }

    public void PlayLaugh()
    {
        Fabric.EventManager.Instance.PostEvent(Laugh);
    }

    public void PlaySelect()
    {
        Fabric.EventManager.Instance.PostEvent(Select);
    }
}

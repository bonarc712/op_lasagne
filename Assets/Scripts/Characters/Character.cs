﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(CharacterSound))]
[RequireComponent(typeof(Rigidbody2D))]
public abstract class Character : MonoBehaviour
{
    public float Speed = 15f;
    public float jumpForce = 1000f;
    public float defaultKnockbackDelay = 0.2f;
    private float currentKnockbackDelay_ = 0;

    public GameObject OpenEyes;
    public GameObject ClosedEyes;
    public GameObject AngryEyes;
    public GameObject DeadEyes;

    public CharacterRig Rig;

    private Transform _groundCheck;

    private enum States { Idle, Walking, Jumping, DoubleJumping, KnockedBack }

    private bool _jumpHasBeenPressed = false;
    private bool _doubleJumpHasBeenPressed = false;

    private States _currentState;

    private bool _isHitting = false;
    private bool _isBlinking = false;
    private bool _isGrounded = false;

    private float _criticalHitTimeRemaining = 0;
    private float _playerDefaultMass;

    // Inputs that are set by the RPC messages
    private bool _jumpInput = false;
    private float _horizontalDirection = 0f;

    private bool _facingRight = false;

    private int _groundMask;

    public bool FacingRight
    {
        get { return _facingRight; }
    }

     WeaponManager weaponManager;

    // The player who's controlling that character
    public Player Player { get; set; }

    private CharacterSound _characterSound;

    protected void Awake()
    {
        _groundCheck = transform.Find("groundCheck");
        weaponManager = this.GetComponentInChildren<WeaponManager>();
        _characterSound = GetComponent<CharacterSound>();

        _groundMask = (1 << LayerMask.NameToLayer("Ground")) | (1 << LayerMask.NameToLayer("passablePlatform"));

        _playerDefaultMass = this.gameObject.rigidbody2D.mass;
    }

    public void Start()
    {
        GameManager.Instance.OnMatchStarted += _characterSound.PlayLaugh;

        //InputManager = Network.Instantiate(InputManager, this.transform.position, Quaternion.identity, 0) as NetworkInput;
    }

    // In case the character dies during the Blink or AngryEyes coroutine
    void OnEnable()
    {
        ClosedEyes.SetActive(false);
        OpenEyes.SetActive(true);
        AngryEyes.SetActive(false);

        _isHitting = false;
        _isBlinking = false;
    }

    public void Reset()
    {
        SetState(States.Idle);
        _characterSound.PlayRevive();
    }

    public bool isHitting()
    {
        return _isHitting;
    }

    public float getHorizontalDirection()
    {
        return _horizontalDirection;
    }

    protected void Update()
    {
        
        if (_criticalHitTimeRemaining > 0)
        {
            rigidbody2D.velocity = rigidbody.velocity.normalized * 100;
            _criticalHitTimeRemaining -= 1 * Time.deltaTime;
        }
        if (_criticalHitTimeRemaining < 0)
        {
            this.gameObject.rigidbody2D.mass = _playerDefaultMass;
            _criticalHitTimeRemaining = 0;
        }
        // This is the ray for the knife range
        Debug.DrawRay(transform.position + Vector3.up, (_facingRight ? Vector2.right : -Vector2.right) * 2f);

        _isGrounded = Physics2D.Linecast(transform.position, _groundCheck.position, _groundMask) && rigidbody2D.velocity.y <= 0;

        if ((_horizontalDirection > 0 && !_facingRight) || (_horizontalDirection < 0 && _facingRight))
        {
            Flip();
        }

        switch (_currentState)
        {
            case States.Idle:
                if (_jumpInput && _isGrounded)
                {
                    SetState(States.Jumping);
                    _jumpHasBeenPressed = true;
                }
                else if (_horizontalDirection != 0)
                {
                    SetState(States.Walking);
                }
                break;
            case States.Walking:
                if (_jumpInput && _isGrounded)
                {
                    SetState(States.Jumping);
                    _jumpHasBeenPressed = true;
                }
                else if (_horizontalDirection == 0)
                {
                    SetState(States.Idle);
                }
                break;
            case States.Jumping:
                if (_isGrounded && !_jumpHasBeenPressed)
                {
                    if (_horizontalDirection == 0)
                    {
                        SetState(States.Idle);
                    }
                    else
                    {
                        SetState(States.Walking);
                    }
                }
                else if (_jumpInput)
                {
                    SetState(States.DoubleJumping);
                    _doubleJumpHasBeenPressed = true;
                }
                break;
            case States.DoubleJumping:
                if (_isGrounded && !_doubleJumpHasBeenPressed)
                {
                    if (_horizontalDirection == 0)
                    {
                        SetState(States.Idle);
                    }
                    else
                    {
                        SetState(States.Walking);
                    }
                }
                break;
            case States.KnockedBack:
                if (currentKnockbackDelay_ == 0)
                {
                    currentKnockbackDelay_ = defaultKnockbackDelay;
                }
                else if (currentKnockbackDelay_ > 0)
                {
                    currentKnockbackDelay_ -= Time.deltaTime;
                }
                else if (currentKnockbackDelay_ < 0)
                {
                    
                    if (_isGrounded)
                    {
                        currentKnockbackDelay_ = defaultKnockbackDelay;
                      //  if (_horizontalDirection == 0)
                      //  {
                      //      SetState(States.Idle);
                      //  }
                      //  else
                       // {
                            SetState(States.Walking);
                        //}
                    }
                    
                }
                //if (_isGrounded)
               // {
                //    if (_horizontalDirection == 0)
               //     {
                //        SetState(States.Idle);
                //    }
                 //   else
                 //   {
                  //      SetState(States.Walking);
                  //  }
                //}
                break;
        }

        if (UnityEngine.Random.Range(0f, 1f) < Time.deltaTime * 0.5f && !_isBlinking && !_isHitting)
        {
            StartCoroutine(Blink());
        }

        if ((_currentState == States.Jumping || _currentState == States.DoubleJumping) && _isGrounded && !_jumpHasBeenPressed)
        {
            if (_horizontalDirection == 0)
            {
                SetState(States.Idle);
            }
            else
            {
                SetState(States.Walking);
            }
        }
    }

    void FixedUpdate()
    {
        // Only modifications to rigidbodies should be handled here (since it needs to be synced with the physics engine), and nothing else

        if (_jumpHasBeenPressed)
        {
            rigidbody2D.AddForce(new Vector2(0f, jumpForce));

            _jumpHasBeenPressed = false;
        }

        if (_doubleJumpHasBeenPressed)
        {
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0);
            rigidbody2D.AddForce(new Vector2(0f, jumpForce));

            _doubleJumpHasBeenPressed = false;
        }
    }

    private IEnumerator Blink()
    {
        _isBlinking = true;

        OpenEyes.SetActive(false);
        ClosedEyes.SetActive(true);

        yield return new WaitForSeconds(0.1f);

        ClosedEyes.SetActive(false);
        OpenEyes.SetActive(true);

        _isBlinking = false;
    }

    private IEnumerator SetAngryEyes()
    {
        _isHitting = true;
        Rig.CharacterAnimator.SetTrigger("Hit1_Trigger");
        _characterSound.PlayAttack();

        AngryEyes.SetActive(true);

        yield return new WaitForSeconds(0.74f);

        AngryEyes.SetActive(false);

        _isHitting = false;
    }

    private IEnumerator executeAttackDuration(float delay)
    {
        yield return new WaitForSeconds(delay);
        AngryEyes.SetActive(false);

        _isHitting = false;
        weaponManager.isWeaponActive(false);
    }

    private void SetState(States newState)
    {
        if (_currentState != newState)
        {
            OnStateChanged(_currentState, newState);
            _currentState = newState;
        }
    }

    private void OnStateChanged(States previousState, States newState)
    {
        switch (previousState)
        {
            case States.Idle:
                Rig.CharacterAnimator.SetBool("IsIdle_Bool", false);
                break;

            case States.Walking:
                Rig.CharacterAnimator.SetBool("IsWalking_Bool", false);
                break;
        }

        switch (newState)
        {
            case States.Idle:
                Rig.CharacterAnimator.SetBool("IsIdle_Bool", true);
                break;
            case States.Walking:
                Rig.CharacterAnimator.SetBool("IsWalking_Bool", true);
                break;
            case States.Jumping:
                // TODO: Good practices: separate animations, sounds and physics into separate components
                Rig.CharacterAnimator.SetBool("Jump_Trigger", true);
                _characterSound.PlayFirstJump();
                _jumpHasBeenPressed = true;
                break;
            case States.DoubleJumping:
                // TODO: Good practices: separate animations, sounds and physics into separate components
                _doubleJumpHasBeenPressed = true;
                _characterSound.PlaySecondJump();
                break;
        }
    }

    public void Jump()
    {
        if (_currentState == States.Idle || _currentState == States.Walking)
        {
            SetState(States.Jumping);
        }
        else if (_currentState == States.Jumping)
        {
            SetState(States.DoubleJumping);
        }
    }

    public void Attack()
    {
        
        if (!_isHitting && weaponManager.CurrentWeaponType != WeaponManager.WeaponTypes.None)
        {
            //StartCoroutine(SetAngryEyes());
            weaponManager.isWeaponActive(true);
            _isHitting = true;

            AngryEyes.SetActive(true);

            _characterSound.PlayAttack();

            

            if (weaponManager.CurrentWeaponType == WeaponManager.WeaponTypes.Ranged)
            {

                Rig.CharacterAnimator.SetTrigger("Shoot1_Trigger");

                weaponManager.fireWeapon();
            }
            else if (weaponManager.CurrentWeaponType == WeaponManager.WeaponTypes.Melee)
            {
                float attackDuration = 1f;
                StartCoroutine(executeAttackDuration(attackDuration));
                //weaponManager.fireWeapon();
                Rig.CharacterAnimator.SetTrigger("Hit1_Trigger");
            }

            

            
        }
    }

    public void receiveHit(Vector2 force)
    {
        rigidbody2D.AddForce(force, ForceMode2D.Impulse);

        SetState(States.KnockedBack);
        _characterSound.PlayHurt();
    }

    public void ReceiveProjectileHit(Vector2 force)
    {
        rigidbody2D.AddForce(force, ForceMode2D.Impulse);

        SetState(States.KnockedBack);
        _characterSound.PlayHurt();
    }

    // When the player receive a critical hit, he is heavier so he fly far forward breaking terrain in his path.
    //this functionnality is not complete. I am not satisfied with how it work for now. I've put it on pause.
    public void ReceiveCriticalHit(Vector2 force, int duration)
    {
        int criticalForce = 1000000;
        // so he can crush all obstacles but the ground
        this.rigidbody2D.mass = criticalForce;
        _criticalHitTimeRemaining = duration;
        rigidbody2D.AddForce(force * criticalForce, ForceMode2D.Impulse);
        SetState(States.KnockedBack);
        _characterSound.PlayHurt();
    }

    
    public void colliderWithRightInvisibleWall(Vector2 force)
    {
        rigidbody2D.AddForce(force, ForceMode2D.Force);
        SetState(States.KnockedBack);
        
    }

	public void Move(float xAxisValue)
	{
        // TODO: Temporary (shoud be fixed with the inputmanager modifications
        if (this == null) return;

        if ((xAxisValue > 0 && !_facingRight) || (xAxisValue < 0 && _facingRight))
        {
            Flip();
        }

        if (_currentState == States.Idle && xAxisValue != 0)
        {
            SetState(States.Walking);
        }
        else if (_currentState == States.Walking && xAxisValue == 0)
        {
            SetState(States.Idle);
        }

        if (_currentState != States.KnockedBack)
        {
            rigidbody2D.velocity = new Vector2(Speed * xAxisValue, rigidbody2D.velocity.y);
        }

		_horizontalDirection = xAxisValue;
    }

    void Flip()
    {
        // Switch the way the player is labelled as facing.
        _facingRight = !_facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void Die()
    {
        _characterSound.PlayDie();
    }

    public void Mock()
    {
        _characterSound.PlayMock();
    }
}



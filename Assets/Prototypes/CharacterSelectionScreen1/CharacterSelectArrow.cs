﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class CharacterSelectArrow : MonoBehaviour
{
    public float LerpDuration = 0.4f;

    private Image _image;
    private Color32 _minColor;
    private Color32 _maxColor;

    private float _ratio = 0f;
    private bool _fadingOut = true;

    void Awake()
    {
        _image = GetComponent<Image>();
        _minColor = _image.color;
        _maxColor = new Color32(_minColor.r, _minColor.g, _minColor.b, 100);
    }
	
	void Update ()
    {
        _ratio += Time.deltaTime / LerpDuration / 2;

        _image.color = _fadingOut ? Color32.Lerp(_maxColor, _minColor, _ratio) : Color32.Lerp(_minColor, _maxColor, _ratio);

        if (_ratio >= 1f)
        {
            _ratio = 0f;
            _fadingOut = !_fadingOut;
        }
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class CharactersCarrousel : MonoBehaviour
{
    [Serializable]
    public struct CharacterBanner
    {
        public Image[] Skins;
        public CharacterRig Avatar;
        public Character Character;
    }

    public CharacterBanner[] CharacterBanners;
    public GameObject[] ArrowButtons;
    public float AnimationDuration = 0.5f;
    public Transform BannerContainer;

    private Transform _transform;

    private int _selectedCharacterIndex = 0;
    private int _selectedSkinIndex = 0;

    private Image _currentBanner;

    private CharacterRig _selectedAvatar;

    public int SelectedCharacterIndex
    {
        get { return _selectedCharacterIndex; }
    }

    public int SelectedSkinIndex
    {
        get { return _selectedSkinIndex; }
    }

    void Awake()
    {
        _transform = GetComponent<Transform>();

        _currentBanner = CreateNewBanner(Vector2.zero, Vector2.one);
        UpdateSelectedAvatar();
    }

    public void SelectNextCharacter(Action callback)
    {
        if (_selectedCharacterIndex == CharacterBanners.Length - 1)
        {
            _selectedCharacterIndex = 0;
        }
        else
        {
            ++_selectedCharacterIndex;
        }

        _selectedSkinIndex = 0;

        Image nextBanner = CreateNewBanner(new Vector2(-1f, 0f), new Vector2(0f, 1f));

        StartCoroutine(SwipeBannerRight(nextBanner, callback));
    }

    public void SelectPreviousCharacter(Action callback)
    {
        if (_selectedCharacterIndex == 0)
        {
            _selectedCharacterIndex = CharacterBanners.Length - 1;
        }
        else
        {
            --_selectedCharacterIndex;
        }

        _selectedSkinIndex = 0;

        Image nextBanner = CreateNewBanner(new Vector2(1f, 0f), new Vector2(2f, 1f));

        StartCoroutine(SwipeBannerLeft(nextBanner, callback));
    }

    public void SelectNextSkin(Action callback)
    {
        if (_selectedSkinIndex == CharacterBanners[_selectedCharacterIndex].Skins.Length - 1)
        {
            _selectedSkinIndex = 0;
        }
        else
        {
            ++_selectedSkinIndex;
        }

        Image nextSkin = CreateNewBanner(new Vector2(0f, -1f), new Vector2(1f, 0f));

        StartCoroutine(SwipeBannerUp(nextSkin, callback));
    }

    public void SelectPreviousSkin(Action callback)
    {
        if (_selectedSkinIndex == 0)
        {
            _selectedSkinIndex = CharacterBanners[_selectedCharacterIndex].Skins.Length - 1;
        }
        else
        {
            --_selectedSkinIndex;
        }

        Image nextSkin = CreateNewBanner(new Vector2(0f, 1f), new Vector2(1f, 2f));

        StartCoroutine(SwipeBannerDown(nextSkin, callback));
    }

    private Image CreateNewBanner(Vector2 minAnchor, Vector2 maxAnchor)
    {
        Image banner = Instantiate(CharacterBanners[_selectedCharacterIndex].Skins[_selectedSkinIndex]) as Image;

        banner.GetComponent<Transform>().SetParent(BannerContainer, false);

        banner.rectTransform.anchorMin = minAnchor;
        banner.rectTransform.anchorMax = maxAnchor;
        banner.rectTransform.offsetMin = Vector2.zero;
        banner.rectTransform.offsetMax = Vector2.zero;

        return banner;
    }

    private IEnumerator SwipeBannerRight(Image nextBanner, Action callback = null)
    {
        // TODO: Maybe merge it with SwipeBannerLeft
        // TODO: Maybe put a mask instead, to reduce the ratioprogression code

        float ratio = 0f;

        Vector2 currentBannerMin = new Vector2(1f, 0f);
        Vector2 currentBannerMax = new Vector2(2f, 1f);

        Vector2 nextBannerMin = nextBanner.rectTransform.anchorMin;
        Vector2 nextBannerMax = nextBanner.rectTransform.anchorMax;

        RatioProgression currentRP = _currentBanner.GetComponent<RatioProgression>();
        RatioProgression nextRP = nextBanner.GetComponent<RatioProgression>();

        currentRP.SetDirection(RatioProgression.Directions.Rightward);
        currentRP.SetRemainingTint(new Color(1f, 1f, 1f, 0f));
        currentRP.SetCompletedTint(new Color(1f, 1f, 1f, 1f));

        nextRP.SetDirection(RatioProgression.Directions.Rightward);
        nextRP.SetRemainingTint(new Color(1f, 1f, 1f, 1f));
        nextRP.SetCompletedTint(new Color(1f, 1f, 1f, 0f));

        while (ratio < 1f)
        {
            ratio += Time.deltaTime / AnimationDuration;

            _currentBanner.rectTransform.anchorMin = Vector2.Lerp(Vector2.zero, currentBannerMin, ratio);
            _currentBanner.rectTransform.anchorMax = Vector2.Lerp(Vector2.one, currentBannerMax, ratio);
            currentRP.SetCompletedRatio(Mathf.Lerp(1f, 0f, ratio));

            nextBanner.rectTransform.anchorMin = Vector2.Lerp(nextBannerMin, Vector2.zero, ratio);
            nextBanner.rectTransform.anchorMax = Vector2.Lerp(nextBannerMax, Vector2.one, ratio);
            nextRP.SetCompletedRatio(Mathf.Lerp(1f, 0f, ratio));

            yield return null;
        }

        Destroy(_currentBanner.gameObject);
        _currentBanner = nextBanner;

        UpdateSelectedAvatar();

        if (callback != null)
        {
            callback();
        }
    }

    private IEnumerator SwipeBannerLeft(Image nextBanner, Action callback = null)
    {
        // TODO: Maybe merge it with SwipeBannerRight
        // TODO: Maybe put a mask instead, to reduce the ratioprogression code

        float ratio = 0f;

        Vector2 currentBannerMin = new Vector2(-1f, 0f);
        Vector2 currentBannerMax = new Vector2(0f, 1f);

        Vector2 nextBannerMin = nextBanner.rectTransform.anchorMin;
        Vector2 nextBannerMax = nextBanner.rectTransform.anchorMax;

        RatioProgression currentRP = _currentBanner.GetComponent<RatioProgression>();
        RatioProgression nextRP = nextBanner.GetComponent<RatioProgression>();

        currentRP.SetDirection(RatioProgression.Directions.Rightward);
        currentRP.SetRemainingTint(new Color(1f, 1f, 1f, 1f));
        currentRP.SetCompletedTint(new Color(1f, 1f, 1f, 0f));

        nextRP.SetDirection(RatioProgression.Directions.Rightward);
        nextRP.SetRemainingTint(new Color(1f, 1f, 1f, 0f));
        nextRP.SetCompletedTint(new Color(1f, 1f, 1f, 1f));

        while (ratio < 1f)
        {
            ratio += Time.deltaTime / AnimationDuration;

            _currentBanner.rectTransform.anchorMin = Vector2.Lerp(Vector2.zero, currentBannerMin, ratio);
            _currentBanner.rectTransform.anchorMax = Vector2.Lerp(Vector2.one, currentBannerMax, ratio);
            currentRP.SetCompletedRatio(Mathf.Lerp(0f, 1f, ratio));

            nextBanner.rectTransform.anchorMin = Vector2.Lerp(nextBannerMin, Vector2.zero, ratio);
            nextBanner.rectTransform.anchorMax = Vector2.Lerp(nextBannerMax, Vector2.one, ratio);
            nextRP.SetCompletedRatio(Mathf.Lerp(0f, 1f, ratio));

            yield return null;
        }

        Destroy(_currentBanner.gameObject);
        _currentBanner = nextBanner;

        UpdateSelectedAvatar();

        if (callback != null)
        {
            callback();
        }
    }

    private IEnumerator SwipeBannerUp(Image nextBanner, Action callback = null)
    {
        // TODO: Maybe merge it with the others
        // TODO: Maybe put a mask instead, to reduce the ratioprogression code

        float ratio = 0f;

        Vector2 currentBannerFinalMinAnchor = new Vector2(0f, 1f);
        Vector2 currentBannerFinalMaxAnchor = new Vector2(1f, 2f);

        Vector2 nextBannerInitialMinAnchor = nextBanner.rectTransform.anchorMin;
        Vector2 nextBannerInitialMaxAnchor = nextBanner.rectTransform.anchorMax;

        RatioProgression currentRP = _currentBanner.GetComponent<RatioProgression>();
        RatioProgression nextRP = nextBanner.GetComponent<RatioProgression>();

        currentRP.SetDirection(RatioProgression.Directions.Upward);
        currentRP.SetRemainingTint(new Color(1f, 1f, 1f, 0f));
        currentRP.SetCompletedTint(new Color(1f, 1f, 1f, 1f));

        nextRP.SetDirection(RatioProgression.Directions.Upward);
        nextRP.SetRemainingTint(new Color(1f, 1f, 1f, 1f));
        nextRP.SetCompletedTint(new Color(1f, 1f, 1f, 0f));

        while (ratio < 1f)
        {
            ratio += Time.deltaTime / AnimationDuration;

            _currentBanner.rectTransform.anchorMin = Vector2.Lerp(Vector2.zero, currentBannerFinalMinAnchor, ratio);
            _currentBanner.rectTransform.anchorMax = Vector2.Lerp(Vector2.one, currentBannerFinalMaxAnchor, ratio);
            currentRP.SetCompletedRatio(Mathf.Lerp(1f, 0f, ratio));

            nextBanner.rectTransform.anchorMin = Vector2.Lerp(nextBannerInitialMinAnchor, Vector2.zero, ratio);
            nextBanner.rectTransform.anchorMax = Vector2.Lerp(nextBannerInitialMaxAnchor, Vector2.one, ratio);
            nextRP.SetCompletedRatio(Mathf.Lerp(1f, 0f, ratio));

            yield return null;
        }

        Destroy(_currentBanner.gameObject);
        _currentBanner = nextBanner;

        UpdateSelectedSkin();

        if (callback != null)
        {
            callback();
        }
    }

    private IEnumerator SwipeBannerDown(Image nextBanner, Action callback = null)
    {
        // TODO: Maybe merge it with the others
        // TODO: Maybe put a mask instead, to reduce the ratioprogression code

        float ratio = 0f;

        Vector2 currentBannerFinalMinAnchor = new Vector2(0f, -1f);
        Vector2 currentBannerFinalMaxAnchor = new Vector2(1f, 0f);

        Vector2 nextBannerInitialMinAnchor = nextBanner.rectTransform.anchorMin;
        Vector2 nextBannerInitialMaxAnchor = nextBanner.rectTransform.anchorMax;

        RatioProgression currentRP = _currentBanner.GetComponent<RatioProgression>();
        RatioProgression nextRP = nextBanner.GetComponent<RatioProgression>();

        currentRP.SetDirection(RatioProgression.Directions.Upward);
        currentRP.SetRemainingTint(new Color(1f, 1f, 1f, 1f));
        currentRP.SetCompletedTint(new Color(1f, 1f, 1f, 0f));

        nextRP.SetDirection(RatioProgression.Directions.Upward);
        nextRP.SetRemainingTint(new Color(1f, 1f, 1f, 0f));
        nextRP.SetCompletedTint(new Color(1f, 1f, 1f, 1f));

        while (ratio < 1f)
        {
            ratio += Time.deltaTime / AnimationDuration;

            _currentBanner.rectTransform.anchorMin = Vector2.Lerp(Vector2.zero, currentBannerFinalMinAnchor, ratio);
            _currentBanner.rectTransform.anchorMax = Vector2.Lerp(Vector2.one, currentBannerFinalMaxAnchor, ratio);
            currentRP.SetCompletedRatio(Mathf.Lerp(0f, 1f, ratio));

            nextBanner.rectTransform.anchorMin = Vector2.Lerp(nextBannerInitialMinAnchor, Vector2.zero, ratio);
            nextBanner.rectTransform.anchorMax = Vector2.Lerp(nextBannerInitialMaxAnchor, Vector2.one, ratio);
            nextRP.SetCompletedRatio(Mathf.Lerp(0f, 1f, ratio));

            yield return null;
        }

        Destroy(_currentBanner.gameObject);
        _currentBanner = nextBanner;

        UpdateSelectedSkin();

        if (callback != null)
        {
            callback();
        }
    }

    private void UpdateSelectedAvatar()
    {
        // TODO: Split into one more "Character Slot" layer when everything is working

        if (_selectedAvatar != null)
        {
            Destroy(_selectedAvatar.gameObject);
        }

        // Ugly, change eventually
        _selectedAvatar = Instantiate(CharacterBanners[_selectedCharacterIndex].Avatar, new Vector3(_transform.position.x, -3.9f, 0f), Quaternion.identity) as CharacterRig;
        _selectedAvatar.transform.localScale = _selectedAvatar.transform.localScale * 0.5f;
    }

    private void UpdateSelectedSkin()
    {
        _selectedAvatar.SetSkin(_selectedSkinIndex);
    }

    public void LockCharacter()
    {
        ChangeAvatarLayer("SelectedAvatar");
        EnableArrows(false);
        _selectedAvatar.SelectSkin();
    }

    public void UnlockCharacter()
    {
        ChangeAvatarLayer("UnselectedAvatar");
        EnableArrows(true);
    }

    private void EnableArrows(bool state)
    {
        foreach (GameObject arrow in ArrowButtons)
        {
            arrow.SetActive(state);
        }
    }

    private void ChangeAvatarLayer(string layerName)
    {
        int selectedLayer = LayerMask.NameToLayer(layerName);

        _selectedAvatar.gameObject.layer = selectedLayer;

        Transform[] children = _selectedAvatar.GetComponentsInChildren<Transform>();

        foreach (Transform child in children)
        {
            child.gameObject.layer = selectedLayer;
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using InputHandler;

public class CharacterSelectionScreen1 : MonoBehaviour
{
    public int MaxPlayersCount = 4;
    public float InputDelay = 0.25f;
    public float InputDeadzone = 0f;

    public CharactersCarrousel[] Carrousels;

    private int[] _lockedCharactersIndexes;
    private int[] _lockedSkinIndexes;
    private bool[] _inputLocked;

    void Awake()
    {
         _lockedCharactersIndexes = new int[MaxPlayersCount];
         _lockedSkinIndexes = new int[MaxPlayersCount];
         _inputLocked = new bool[MaxPlayersCount];

        for (int i = 0; i < MaxPlayersCount; i++)
		{
            _lockedCharactersIndexes[i] = -1;

            // TODO: Push a unique context per player ("CharacterSelection" + i) for keyboard compatibility
            InputManager.Instance.PushActiveContext("CharacterSelection", i);
            InputManager.Instance.AddCallback(i, HandleSelection);
            InputManager.Instance.AddCallback(i, HandleLock);
		}
    }

    private void HandleSelection(MappedInput input)
    {
        if (_inputLocked[input.PlayerIndex] || _lockedCharactersIndexes[input.PlayerIndex] != -1) return;

        float range = 0f;

        if (input.Ranges.ContainsKey("SelectNextCharacter"))
        {
            range = input.Ranges["SelectNextCharacter"];

            if (range >= InputDeadzone)
            {
                SelectNextCharacter(input.PlayerIndex);
            }
        }
        else if (input.Ranges.ContainsKey("SelectPreviousCharacter"))
        {
            range = input.Ranges["SelectPreviousCharacter"];

            if (range >= InputDeadzone)
            {
                SelectPreviousCharacter(input.PlayerIndex);
            }
        }
        else if (input.Ranges.ContainsKey("SelectNextSkin"))
        {
            range = input.Ranges["SelectNextSkin"];

            if (range >= InputDeadzone)
            {
                SelectNextSkin(input.PlayerIndex);
            }
        }
        else if (input.Ranges.ContainsKey("SelectPreviousSkin"))
        {
            range = input.Ranges["SelectPreviousSkin"];

            if (range >= InputDeadzone)
            {
                SelectPreviousSkin(input.PlayerIndex);
            }
        }
    }

    private void HandleLock(MappedInput input)
    {
        if (_inputLocked[input.PlayerIndex]) return;

        if (input.Actions.Contains("LockCurrentCharacter") && _lockedCharactersIndexes[input.PlayerIndex] == -1)
        {
            LockCurrentCharacter(input.PlayerIndex);
        }

        if (input.Actions.Contains("UnlockCurrentCharacter") && _lockedCharactersIndexes[input.PlayerIndex] != -1)
        {
            UnlockCurrentCharacter(input.PlayerIndex);
        }

        if (input.Actions.Contains("StartLevel"))
        {
            StartLevel();
        }
    }

    private void SelectNextCharacter(int playerIndex)
    {
        _inputLocked[playerIndex] = true;
        Carrousels[playerIndex].SelectNextCharacter(() => { _inputLocked[playerIndex] = false; });
    }

    private void SelectPreviousCharacter(int playerIndex)
    {
        _inputLocked[playerIndex] = true;
        Carrousels[playerIndex].SelectPreviousCharacter(() => { _inputLocked[playerIndex] = false; });
    }

    private void SelectNextSkin(int playerIndex)
    {
        _inputLocked[playerIndex] = true;
        Carrousels[playerIndex].SelectNextSkin(() => { _inputLocked[playerIndex] = false; });
    }

    private void SelectPreviousSkin(int playerIndex)
    {
        _inputLocked[playerIndex] = true;
        Carrousels[playerIndex].SelectPreviousSkin(() => { _inputLocked[playerIndex] = false; });
    }

    private void LockCurrentCharacter(int playerIndex)
    {
        int charIndex = Carrousels[playerIndex].SelectedCharacterIndex;
        int skinIndex = Carrousels[playerIndex].SelectedSkinIndex;

        for (int i = 0; i < _lockedCharactersIndexes.Length; i++)
        {
            // TODO: Add some kind of negative confirmation that the character can't be selected
            if (_lockedCharactersIndexes[i] == charIndex && _lockedSkinIndexes[i] == skinIndex)
            {
                return;
            }
        }

        _lockedCharactersIndexes[playerIndex] = charIndex;
        _lockedSkinIndexes[playerIndex] = skinIndex;
        
        // We add the lighting to the character
        Carrousels[playerIndex].LockCharacter();
    }

    private void UnlockCurrentCharacter(int playerIndex)
    {
        _lockedCharactersIndexes[playerIndex] = -1;

        // We remove the lighting from the character
        Carrousels[playerIndex].UnlockCharacter();
    }

    private void StartLevel()
    {
        int playerCount = 0;

        foreach (int index in _lockedCharactersIndexes)
        {
            if (index != -1)
            {
                ++playerCount;
            }
        }

        if (playerCount >= 2)
        {
            Character[] characters = new Character[4];
            int[] skins = new int[4];

            for (int i = 0; i < Carrousels.Length; i++)
            {
                if (_lockedCharactersIndexes[i] == -1) continue;

                int avatarIndex = _lockedCharactersIndexes[i];
                int skinIndex = _lockedSkinIndexes[i];

                characters[i] = Carrousels[i].CharacterBanners[avatarIndex].Character;
                skins[i] = skinIndex;
            }

            GameManager.Instance.StartLevel(characters, skins);
        }
        else
        {
            // TODO: Show an error message indicating to the player that he needs one more players to play
        }
        
    }
}
